#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "SAWs.h"

/*
     This directly tests implementation routines in SAWs, thus we need access to the following definitions

     This is absolutely terrible because it repeats exactly the definitions in SAWs.c but I wanted to keep 
     all the SAWs source in one file for user simplicity.  

     We need to fix this somehow
*/


typedef struct _P_SAWs_Variable* SAWs_Variable;
typedef struct _P_SAWs_Directory* SAWs_Directory;
extern SAWs_Directory SAWs_ROOT_DIRECTORY;

struct _P_SAWs_Variable
{
  char             *name;
  void             *data;            /* raw data  */
  int              len;
  SAWs_Memory_type mtype;
  SAWs_Data_type   dtype;
  int              status;           /* 0 = unmodified, 1 = modified */
  SAWs_Variable    prev;             /* pointer to previous added variable */
  SAWs_Variable    next;             /* pointer to next added variable */
  void             *alternatives;
  int              altLen;
};

struct _P_SAWs_Directory
 {
   char            *name;
   SAWs_Variable   variables;
   SAWs_Variable   tailVariable;
   int             nb_variables;
   SAWs_Directory  firstChild,nextSibling,parent;
   pthread_mutex_t dir_Lock;
};

/* SAWs operations */
int SAWs_Add_Variable(SAWs_Directory,const char*,void*,int,SAWs_Memory_type,SAWs_Data_type);
int SAWs_Add_Directory(SAWs_Directory,const char*);
int SAWs_Destroy_Directory(SAWs_Directory*);
int SAWs_Lock_Directory(SAWs_Directory);
int SAWs_Unlock_Directory(SAWs_Directory);

 /*cstruct to JSON parser*/
int JSONParser_CStruct_to_JSON(char*,int);
int JSONParser_Single_CStruct_to_JSON(char*,SAWs_Directory,int);
int JSONParser_Single_Variable_CStruct_to_JSON(char*,SAWs_Directory,char*,int);
int JSONParser_Single_All_Variable_CStruct_to_JSON(char*,SAWs_Directory,int);

/*json to cStruct parser*/
int cJSON_to_CStruct_Parser(char*,char*,char*);

/*Directory search*/
int _P_SAWs_Directory_Search(SAWs_Directory*,char*);
int _P_SAWs_Create_Variable(SAWs_Variable*);
int _P_SAWs_Free_Variable_List(SAWs_Variable);
int _P_SAWs_Search_Directory_Pointer(SAWs_Directory currentDirectory,SAWs_Directory desiredDirectory);
int _P_SAWs_Directory_One_Level_Search(SAWs_Directory *dir,const char *dirName,int levels);
int _P_SAWs_Search_Variables(SAWs_Directory dir,const char *name);

int main(int argc,char **argv)
{ 
  /*Make up some data*/
  char        *final_JSON;
  int         length               = 3;
  char        **stringData         = malloc(sizeof(char*));
  char        **stringDataCopy     = malloc(sizeof(char*));
  char        **stringDataArray    = malloc(length * sizeof(char*));
  char        **stringDataArrayCopy= malloc(length * sizeof(char*));
  int         *intData             = malloc(sizeof(int));
  int         *intDataArray        = malloc(length * sizeof(int)); 
  int         *intDataCopy         = malloc(sizeof(int));
  int         *intDataArrayCopy    = malloc(length * sizeof(int));
  double      *doubleData          = malloc(sizeof(double));
  double      *doubleDataArray     = malloc(length * sizeof(double));
  double      *doubleDataCopy      = malloc(sizeof(double));
  double      *doubleDataArrayCopy = malloc(length * sizeof(double));
  float       *floatData           = malloc(sizeof(float));
  float       *floatDataArray      = malloc(length * sizeof(float));
  float       *floatDataCopy       = malloc(sizeof(float));
  float       *floatDataArrayCopy  = malloc(length * sizeof(float));
  char        *charData            = malloc(sizeof(char));
  char        *charDataArray       = malloc(length * sizeof(char));
  char        *charDataCopy        = malloc(sizeof(char));
  char        *charDataArrayCopy   = malloc(length * sizeof(char));
  int         *boolData            = malloc(sizeof(int)),i = 0;
  int         *boolDataArray       = malloc(length * sizeof(int));
  int         *boolDataCopy        = malloc(sizeof(int));
  int         *boolDataArrayCopy   = malloc(length * sizeof(int));
  

  *boolData       = 1;
  *boolDataCopy   = 1;
  *intData        = 41;
  *intDataCopy    = 41;
  *doubleData     = 1.234567;
  *doubleDataCopy = 1.234567;
  *floatData      = 7.65432;
  *floatDataCopy  = 7.65432;
  *charData       = 'b';
  *charDataCopy   = 'b';
  *stringData     = strdup("string");
  *stringDataCopy = "string";

  for(i=0;i<length;i++){
    boolDataArray[i]       = 1;
    boolDataArrayCopy[i]   = 1;
    intDataArray[i]        = i + 12;
    intDataArrayCopy[i]    = i + 12;
    doubleDataArray[i]     = 27.7 + i;
    doubleDataArrayCopy[i] = doubleDataArray[i];
    floatDataArray[i]      = 13.3 + i;
    floatDataArrayCopy[i]  = 13.3 + i;
    charDataArray[i]       = 'b';
    charDataArrayCopy[i]   = 'b';
    stringDataArray[i]     = strdup("string");
    stringDataArrayCopy[i] = "string";
  }


  SAWs_Initialize();
  /* Declare a directory */
  SAWs_Variable fld;

  /* add a variable */
  SAWs_Add_Variable(SAWs_ROOT_DIRECTORY,"residual",boolData,1,SAWs_WRITE,SAWs_BOOLEAN);
  SAWs_Add_Variable(SAWs_ROOT_DIRECTORY,"residual2",stringData,1,SAWs_WRITE,SAWs_STRING);
  SAWs_Add_Variable(SAWs_ROOT_DIRECTORY,"residual3",doubleData,1,SAWs_WRITE,SAWs_DOUBLE);
  SAWs_Add_Variable(SAWs_ROOT_DIRECTORY,"residual4",intData,1,SAWs_WRITE,SAWs_INT);
  SAWs_Add_Variable(SAWs_ROOT_DIRECTORY,"residual5",floatData,1,SAWs_WRITE,SAWs_FLOAT);
  SAWs_Add_Variable(SAWs_ROOT_DIRECTORY,"residual6",charData,1,SAWs_WRITE,SAWs_CHAR);
  SAWs_Add_Variable(SAWs_ROOT_DIRECTORY,"residual7",boolDataArray,length,SAWs_WRITE,SAWs_BOOLEAN);
  SAWs_Add_Variable(SAWs_ROOT_DIRECTORY,"residual8",stringDataArray,length,SAWs_WRITE,SAWs_STRING);
  SAWs_Add_Variable(SAWs_ROOT_DIRECTORY,"residual9",doubleDataArray,length,SAWs_WRITE,SAWs_DOUBLE);
  SAWs_Add_Variable(SAWs_ROOT_DIRECTORY,"residual10",intDataArray,length,SAWs_WRITE,SAWs_INT);
  SAWs_Add_Variable(SAWs_ROOT_DIRECTORY,"residual11",floatDataArray,length,SAWs_WRITE,SAWs_FLOAT);
  SAWs_Add_Variable(SAWs_ROOT_DIRECTORY,"residual12",charDataArray,length,SAWs_WRITE,SAWs_CHAR);


  int json_Length = 409600;
  final_JSON = malloc(409600);
  /* parse the variable */
  JSONParser_CStruct_to_JSON(final_JSON,json_Length);
  printf("%s\n",final_JSON);

  cJSON_to_CStruct_Parser(final_JSON,NULL,NULL);
  //printf("%s\n",final_JSON);
  JSONParser_CStruct_to_JSON(final_JSON,json_Length);
  //printf("%s\n",final_JSON);
  cJSON_to_CStruct_Parser(final_JSON,NULL,NULL);
  JSONParser_CStruct_to_JSON(final_JSON,json_Length);
  //printf("%s\n",final_JSON);

  fld = SAWs_ROOT_DIRECTORY->variables;

  /*check each data mem`ber against its copy*/
  if(*(int*)fld->data != *boolDataCopy) printf("Single Boolean broken \n");
  fld = fld->next;
  if(strcmp(*(char**)fld->data,*stringDataCopy)) printf("Single String broken: variable: %s copy: %s\n", *(char**)fld->data, *stringDataCopy);
  fld = fld->next;
  if(*(double*)fld->data != *doubleDataCopy) printf("Single Double broken \n");
  fld = fld->next;
  if(*(int*)fld->data != *intDataCopy) printf("Single Int broken \n");
  fld = fld->next;
  if(*(float*)fld->data != *floatDataCopy) printf("Single Float broken \n");
  fld = fld->next;
  if(*(char*)fld->data != *charDataCopy) printf("Single Char broken: variable: %c copy: %c\n", *(char*)fld->data, *charDataCopy);
  fld = fld->next;




  for(i=0;i<length;i++){
    if(((int*)fld->data)[i] != boolDataArrayCopy[i]) printf("Array number: %d Boolean broken \n",i);
  }
  fld = fld->next;
  for(i=0;i<length;i++){
    if(strcmp(*(char**)fld->data,*stringDataCopy)) printf("Array number: %d String broken: variable: %s copy: %s\n",i,*(char**)fld->data,*stringDataCopy);
  }
  fld = fld->next;
  for(i=0;i<length;i++){
    if(((double*)fld->data)[i] != doubleDataArrayCopy[i]) printf("Array number: %d Double broken. Variable: %f copy: %f \n",i,((double*)fld->data)[i],doubleDataArrayCopy[i]);
  }  
  fld = fld->next;
  for(i=0;i<length;i++){
    if(((int*)fld->data)[i] != intDataArrayCopy[i]) printf("Array number: %d Int broken \n",i);
  } 
  fld = fld->next;
  for(i=0;i<length;i++){
    if(((float*)fld->data)[i] != floatDataArrayCopy[i]) printf("Array number: %d Float broken \n",i);
  }
  fld = fld->next;
  for(i=0;i<length;i++){
    if(((char*)fld->data)[i] != charDataArrayCopy[i]) printf("Array number: %d Char broken variable: %c copy %c\n",i,((char*)fld->data)[i],charDataArrayCopy[i]);
  }



  free(final_JSON);
  SAWs_Finalize();

  /*free all the user data*/
  free(intData);
  free(intDataArray);
  free(intDataCopy);
  free(intDataArrayCopy);
  free(*stringData);
  free(stringData);
  free(stringDataCopy);
  free(stringDataArrayCopy);
  for(i=0;i<length;i++){
    free(stringDataArray[i]);
  }
  free(stringDataArray);
  free(doubleData);
  free(doubleDataArray);
  free(doubleDataArrayCopy);
  free(doubleDataCopy);
  free(floatData);
  free(floatDataArray);
  free(floatDataCopy);
  free(floatDataArrayCopy);
  free(charData);
  free(charDataArray);
  free(charDataCopy);
  free(charDataArrayCopy);
  free(boolData);
  free(boolDataArray);
  free(boolDataCopy);
  free(boolDataArrayCopy);

  return 0;
}

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "SAWs.h"

/*includes SAWsprivate because we are testing private functions*/
int main(int argc,char **argv)
{ 
  /*Make up some data*/
  int         length               = 3;
  char        **stringData         = malloc(sizeof(char*));
  char        **stringDataArray    = malloc(length * sizeof(char*));
  int         *intData             = malloc(sizeof(int));
  int         *intDataArray        = malloc(length * sizeof(int)); 
  double      *doubleData          = malloc(sizeof(double));
  double      *doubleDataArray     = malloc(length * sizeof(double));
  float       *floatData           = malloc(sizeof(float));
  float       *floatDataArray      = malloc(length * sizeof(float));
  char        *charData            = malloc(sizeof(char));
  char        *charDataArray       = malloc(length * sizeof(char));
  int         *boolData            = malloc(sizeof(int)),i = 0;
  int         *boolDataArray       = malloc(length * sizeof(int));

  *boolData       = 1;
  *intData        = 41;
  *doubleData     = 1.234567;
  *floatData      = 7.65432;
  *charData       = 'b';
  *stringData     = strdup("string");

  for(i=0;i<length;i++){
    boolDataArray[i]       = 1;
    intDataArray[i]        = i + 12;
    doubleDataArray[i]     = 27.7 + i;
    floatDataArray[i]      = 13.3 + i;
    charDataArray[i]       = 'b';
    stringDataArray[i]     = strdup("string");
  }


  SAWs_Initialize();
  /* Declare a directory */

  /* add a variable */
  SAWs_Register("dir1/dir2/dir3/residual",boolData,1,SAWs_WRITE,SAWs_BOOLEAN);
  SAWs_Register("dir2/dir5/dir4/residual",stringData,1,SAWs_WRITE,SAWs_STRING);
  SAWs_Register("dir3/residual",doubleData,1,SAWs_WRITE,SAWs_DOUBLE);
  SAWs_Register("dir1/dir2/residual4",intData,1,SAWs_WRITE,SAWs_INT);
  SAWs_Register("dir1/residual5",floatData,1,SAWs_WRITE,SAWs_FLOAT);
  SAWs_Register("residual6",charData,1,SAWs_WRITE,SAWs_CHAR);
  SAWs_Register("dir1/dir2/dir3/residual7",boolDataArray,length,SAWs_WRITE,SAWs_BOOLEAN);
  SAWs_Register("residual8",stringDataArray,length,SAWs_WRITE,SAWs_STRING);
  SAWs_Register("dir2/dir3/dir4/residual9",doubleDataArray,length,SAWs_WRITE,SAWs_DOUBLE);
  SAWs_Register("dir3/residual10",intDataArray,length,SAWs_WRITE,SAWs_INT);
  SAWs_Register("dir1/residual11",floatDataArray,length,SAWs_WRITE,SAWs_FLOAT);
  SAWs_Register("residual12",charDataArray,length,SAWs_WRITE,SAWs_CHAR);

  getchar();

  SAWs_Delete("dir1");
  SAWs_Delete("dir2/dir3");
  SAWs_Delete("residual8");
  SAWs_Delete("residual12");
  
  /*Try to delete something already deleted*/
  SAWs_Delete("dir");

  /*Try to delete garbage*/
  SAWs_Delete("gar/bag/e");
  

  SAWs_Finalize();

  /*free all the user data*/
  free(intData);
  free(intDataArray);
  free(*stringData);
  free(stringData);
  for(i=0;i<length;i++){
    free(stringDataArray[i]);
  }
  free(stringDataArray);
  free(doubleData);
  free(doubleDataArray);
  free(floatData);
  free(floatDataArray);
  free(charData);
  free(charDataArray);
  free(boolData);
  free(boolDataArray);

  return 0;
}
